//
// Created by Dominik on 17.10.2015.
//

#ifndef AUTOMAT_STATEBASE_H
#define AUTOMAT_STATEBASE_H


#include "../../../Token/includes/Token.h"

class FSM; //forward declaration, because StateBase cross references with FSM
/**
 * StateBase is a abstract base class for states used in the FSM. It provides a method
 * to read a specific character which triggers transitions into following states.
 * \author Dominik Bartos
 */
class StateBase {
public:

    /**
     * Destroys the state and releases used resources
     */
   virtual ~StateBase(){};

    /**
     * Reads a specific char and triggers the corresponding transitions in the following state.
     * Also calls createToken() and ungetChar() on the FSM if necessary
     * @param c the character that is read in by the state/FSM
     */
    virtual void readChar(char c) = 0;
    void setStateNr(int nr){
        this->stateNr = nr;
    }


protected:
    /**
     * Stores the FSM which uses this state when a more concrete State which inherits StateBase
     * is created
     */
    StateBase(FSM* fsm){

        this->fsm = fsm;
    }

    /**
     * the number of the state.
     */
    int stateNr = 0;
    /**
     * The FSM in which the specific state is used.
     */
    FSM* fsm;





};


#endif //AUTOMAT_STATEBASE_H
