//
// Created by Dominik on 22.10.2015.
//

#ifndef AUTOMAT_FSMTESTER_H
#define AUTOMAT_FSMTESTER_H


#include "../LFSM.h"
#include "../../../Buffer/includes/Buffer.h"
#include "../../../Token/includes/TokenBuilder.h"
#include "../../../Scanner/includes/IScanner.h"

/**
 * A class that implements scanner interface to test the FSM.
 * It contains several methods to compare the result with the expected result.
 * \author Dominik Bartos
 */
class FSMTester : public IScanner{
public:
    char* testName;
    bool testMode;

    /**
     * Creates a new instance of the FSMTester
     */
    FSMTester();

    /**
     * Destroys the FSMTester
     */
    ~FSMTester();

    /**
     * Sets the token that is expected as result while processing the following input
     */
    void expectedToken(Tokens::TokenCategory expectedTokenType);

/**
 * sets the number of chars that is expected while processing the following input
 */
    void expectedUngetChars(int nr);

    /**
     * Sets if an error is expected while processing the given input
     */
    void expectedError(bool errorExpected);


	/**
	 * Creates a new token out of the given TokenCategory.
	 * @param TokenType the category of the token which should be created
	 */
    virtual void createToken(Tokens::TokenCategory tokenType) override;

    /**
     * Indicates to go a specific number of chars back
     * @param nr the number of chars to go back
     */
    virtual void ungetChar(int nr) override;

    /**
     * A method to test the fsm with an buffer object instead of manual input.
     */
    virtual void testWithBuffer();

    /**
     * Sets a specific lfsm to test
     */
    virtual void setLfsm(LFSM* lfsm);

    /**
     * Processes given characters and returns a token if a token is found.
     * @return a token, if found
     */
    Token * nextToken();

private:
    int expectedUngetChar;
    bool excpectedError;
    Tokens::TokenCategory expectedTokenType;
    LFSM* lfsm;
    Buffer *buffer;
    TokenBuilder* tokenBuilder;






};

#endif //AUTOMAT_FSMTESTER_H
