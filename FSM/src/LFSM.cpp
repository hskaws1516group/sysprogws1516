//
// Created by Dominik on 21.10.2015.
//




#include <iostream>

#include "../includes/FSM.h"
#include "../includes/LFSM.h"
#include "../includes/States/DefaultStates.h"
#include "../includes/States/SignStates.h"
#include "../includes/States/DigitLetterStates.h"

LFSM::LFSM(IScanner *scanner) : FSM() {

    this->scanner = scanner;

    states[0] = new InitState(this);
    states[1] = new ErrorState(this);
    states[2] = new StopState(this);
    states[3] = new State03(this);
    states[4] = new State04(this);
    states[5] = new State05(this);
    states[6] = new State06(this);
    states[7] = new State07(this);
    states[8] = new State08(this);
    states[9] = new State09(this);
    states[10] = new State10(this);
    states[11] = new State11(this);
    states[12] = new State12(this);
    states[13] = new State13(this);
    states[14] = new SpacesBreaksCharState(this);

    for (int i = 0; i < stateCount; ++i) {
        states[i]->setStateNr(i);
    }

    currentState = states[0];
    linePosHistoryCounter = 0;
    linePosHistory = new int[9];
    linePosition = 1;
    rowPosHistory = new int[9];
    rowPosHistoryCounter = 0;
    rowCount = 1;


}

LFSM::~LFSM() {



    //free memory
    for (int i = 0; i < stateCount; ++i) {
        delete states[i];
        states[i] = 0;
    }
    delete[] states;
    delete[] linePosHistory;
    delete[] rowPosHistory;
    *states = 0;
}

void LFSM::reset() {
    currentState = states[0];
    rowCount = 1;
    rowPosHistoryCounter = 0;
    linePosition = 1;
    linePosHistoryCounter = 0;
}

StateBase *LFSM::provideState(int stateNr) {
    if (stateNr >= 0 && stateNr < stateCount) {
        return states[stateNr];
    }

    return states[0];
}

void LFSM::ungetChar(int nr) {

    for (int i = 0; i < nr+1; ++i) {
        linePosHistoryCounter--;
        if(linePosHistoryCounter < 0){

            throw "FSM: LinePosHistoryCounter is negative!";
        }

        linePosition = linePosHistory[linePosHistoryCounter % 9];


        rowPosHistoryCounter--;
        if(rowPosHistoryCounter < 0){

            throw "FSM: RowPosHistoryCounter is negative!";
        }
        rowCount = rowPosHistory[rowPosHistoryCounter % 9];
    }
   // printf("unget %i column: %i row: %i\n",nr, linePosition, rowCount);
    tokenLength -= nr;
    scanner->ungetChar(nr);
}

 void LFSM::makeToken(Tokens::TokenCategory tokenType) {

   scanner->createToken(tokenType);

    tokenLength = 0;

}



void LFSM::readChar(char c) {

    int asciiCode = (unsigned char) c;

  if (asciiCode > 31) {

      linePosition++;

    }
    if(asciiCode == 10){ //unix new line

        rowCount++;

        linePosition = 1;
    }
    linePosHistory[(linePosHistoryCounter % 9)] = linePosition;
    linePosHistoryCounter++;

    rowPosHistory[(rowPosHistoryCounter % 9)] = rowCount;
    rowPosHistoryCounter++;

    tokenLength++;



    //provide the read char to the specific states
    FSM::readChar(c);
}


int LFSM::getActualTokenLength() {
    return tokenLength;
}

int LFSM::getRowCount() {
    return rowCount;
}

int LFSM::getLinePosition() {
    return linePosition-tokenLength;
}


