#include <iostream>

#include "../includes/LFSM.h"
#include "../includes/TestFSM/FSMTester.h"
#include <chrono> //for time measurement


LFSM* lfsm;
FSMTester* fsmTester;

void testSign01(){
    fsmTester->testName = "Sign01";
    fsmTester->expectedToken(SP::SIGN);
    fsmTester->expectedUngetChars(1);
    fsmTester->expectedError(false);
        lfsm->reset();
    lfsm->readChar('+');
    lfsm->readChar('c');
    lfsm->reset();
    lfsm->readChar('-');
    lfsm->readChar('+');
    lfsm->reset();
    lfsm->readChar('*');
    lfsm->readChar('2');
    lfsm->reset();
    lfsm->readChar('>');
    lfsm->readChar('c');
    lfsm->reset();
    lfsm->readChar('=');
    lfsm->readChar('q');
    lfsm->reset();
    lfsm->readChar('!');
    lfsm->readChar('f');
    lfsm->reset();
    lfsm->readChar('&');
    lfsm->readChar('/');
    lfsm->reset();
    lfsm->readChar(';');
    lfsm->readChar('@');
    lfsm->reset();
    lfsm->readChar('(');
    lfsm->readChar('"');
    lfsm->reset();
    lfsm->readChar(')');
    lfsm->readChar('c');
    lfsm->reset();
    lfsm->readChar('{');
    lfsm->readChar('+');
    lfsm->reset();
    lfsm->readChar('}');
    lfsm->readChar('+');
    lfsm->reset();
    lfsm->readChar('[');
    lfsm->readChar('+');
    lfsm->reset();
    lfsm->readChar(']');
    lfsm->readChar('+');

}

void testSign02(){
    fsmTester->testName = "Sign02";
    fsmTester->expectedToken(SP::SIGN);
    fsmTester->expectedUngetChars(1);
    fsmTester->expectedError(false);
    lfsm->reset();

    lfsm->readChar('<');
    lfsm->readChar(':');
    lfsm->readChar('>');
    lfsm->readChar('+');
    lfsm->reset();
}

void testSign03(){
    fsmTester->testName = "Sign03";
    fsmTester->expectedToken(SP::SIGN);
    fsmTester->expectedUngetChars(1);
    fsmTester->expectedError(false);
    lfsm->reset();
    lfsm->readChar(':');
    lfsm->readChar('=');
    lfsm->readChar('c');
    lfsm->reset();
}

void testSign04(){
    fsmTester->testName = "Sign04";
    fsmTester->expectedToken(SP::SIGN);
    fsmTester->expectedUngetChars(1);
    fsmTester->expectedError(false);
    lfsm->reset();

    lfsm->readChar('<');
    lfsm->readChar('+');
    lfsm->reset();
}

void testSign05(){
    fsmTester->testName = "Sign05";
    fsmTester->expectedToken(SP::SIGN);
    fsmTester->expectedUngetChars(1);
    fsmTester->expectedError(false);
    lfsm->reset();

    lfsm->readChar(':');
    lfsm->readChar('+');
    lfsm->reset();
}

void testSign06(){
    fsmTester->testName = "Sign06";
    fsmTester->expectedToken(SP::SIGN);
    fsmTester->expectedUngetChars(2);
    fsmTester->expectedError(false);
    lfsm->reset();

    lfsm->readChar('<');
    lfsm->readChar(':');
    lfsm->readChar('c');
    lfsm->reset();
}

void testSign07(){
    fsmTester->testName = "Sign07";
    fsmTester->expectedToken(SP::NOTOKEN);
    fsmTester->expectedUngetChars(1);
    fsmTester->expectedError(false);
    lfsm->reset();

    lfsm->readChar(':');
    lfsm->readChar('*');
    lfsm->readChar('c');
    lfsm->readChar('t');
    lfsm->readChar('@');
    lfsm->readChar('g');
    lfsm->readChar('�');
    lfsm->readChar('*');
    lfsm->readChar(':');
    lfsm->readChar('n');
    lfsm->reset();
}

void testSign08(){
    fsmTester->testName = "Sign08";
    fsmTester->expectedToken(SP::NOTOKEN);
    fsmTester->expectedUngetChars(1);
    fsmTester->expectedError(false);
    lfsm->reset();

    lfsm->readChar(':');
    lfsm->readChar('*');
    lfsm->readChar('\0');
    lfsm->readChar('a');

    lfsm->reset();
}


void testSign09(){
    fsmTester->testName = "Sign09";
    fsmTester->expectedToken(SP::NOTOKEN);
    fsmTester->expectedUngetChars(1);
    fsmTester->expectedError(false);
    lfsm->reset();

    lfsm->readChar(':');
    lfsm->readChar('*');
    lfsm->readChar('*');
    lfsm->readChar('\0');
    lfsm->readChar('a');

    lfsm->reset();
}

void testDigit01(){
    fsmTester->testName = "Digit01";
    fsmTester->expectedToken(SP::INTEGER);
    fsmTester->expectedUngetChars(1);
    fsmTester->expectedError(false);
    lfsm->reset();
    lfsm->readChar('0');
    lfsm->readChar('1');
    lfsm->readChar('9');
    lfsm->readChar('6');
    lfsm->readChar('g');

}

void testDigit02(){
    fsmTester->testName = "Digit02";
    fsmTester->expectedToken(SP::INTEGER);
    fsmTester->expectedUngetChars(1);
    fsmTester->expectedError(false);
    lfsm->reset();
    lfsm->readChar('0');
    lfsm->readChar('1');
    lfsm->readChar('9');
    lfsm->readChar('6');
    lfsm->readChar('+');
}

void testIdentifier01(){
    fsmTester->testName = "Identifier01";
    fsmTester->expectedToken(SP::IDENTIFIER);
    fsmTester->expectedUngetChars(1);
    fsmTester->expectedError(false);
    lfsm->reset();
    lfsm->readChar('a');
    lfsm->readChar('+');
}

void testIdentifier02(){
    fsmTester->testName = "Identifier02";
    fsmTester->expectedToken(SP::IDENTIFIER);
    fsmTester->expectedUngetChars(1);
    fsmTester->expectedError(false);
    lfsm->reset();
    lfsm->readChar('a');
    lfsm->readChar('z');
    lfsm->readChar('(');
}

void testIdentifier03(){
    fsmTester->testName = "Identifier03";
    fsmTester->expectedToken(SP::IDENTIFIER);
    fsmTester->expectedUngetChars(1);
    fsmTester->expectedError(false);
    lfsm->reset();
    lfsm->readChar('a');
    lfsm->readChar('z');
    lfsm->readChar('1');
    lfsm->readChar('+');
}

void test01(){
    fsmTester->testName = "test01";
    fsmTester->expectedToken(SP::SIGN);
    fsmTester->expectedUngetChars(1);
    fsmTester->expectedError(false);
    lfsm->reset();
    lfsm->readChar('+');
    lfsm->readChar('z');
    fsmTester->expectedToken(SP::IDENTIFIER);
    fsmTester->expectedUngetChars(1);
    fsmTester->expectedError(false);
    lfsm->readChar('z');//simulate ungetChar with second read with same char
    lfsm->readChar('b');
    lfsm->readChar('1');
    lfsm->readChar('+');

}

void test02(){
    fsmTester->testName = "test02";
    fsmTester->expectedToken(SP::SIGN);
    fsmTester->expectedUngetChars(1);
    fsmTester->expectedError(false);
    lfsm->reset();
    lfsm->readChar('<');
    lfsm->readChar('a');
    fsmTester->expectedToken(SP::IDENTIFIER);
    fsmTester->expectedUngetChars(1);
    fsmTester->expectedError(false);
    lfsm->readChar('a');//simulate ungetChar with second read with same char
    lfsm->readChar('b');
    lfsm->readChar('1');
    lfsm->readChar('+');

}

void test03(){
    fsmTester->testName = "test03";
    fsmTester->expectedToken(SP::SIGN);
    fsmTester->expectedUngetChars(2);
    fsmTester->expectedError(false);
    lfsm->reset();
    lfsm->readChar('<');
    lfsm->readChar(':');
    lfsm->readChar('a');
    fsmTester->expectedToken(SP::IDENTIFIER);
    fsmTester->expectedUngetChars(1);
    fsmTester->expectedError(false);
    lfsm->readChar('a');//simulate ungetChar with second read with same char
    lfsm->readChar('b');
    lfsm->readChar('1');
    lfsm->readChar('+');

}

void test04(){
    fsmTester->testName = "test04";
    fsmTester->expectedToken(SP::SIGN);
    fsmTester->expectedUngetChars(1);
    fsmTester->expectedError(false);
    lfsm->reset();

    lfsm->readChar(':');
    lfsm->readChar('a');
    fsmTester->expectedToken(SP::IDENTIFIER);
    fsmTester->expectedUngetChars(1);
    fsmTester->expectedError(false);
    lfsm->readChar('a');//simulate ungetChar with second read with same char
    lfsm->readChar('b');
    lfsm->readChar('1');
    lfsm->readChar('+');

}

void test05(){
    fsmTester->testName = "test05";
    fsmTester->expectedToken(SP::SIGN);
    fsmTester->expectedUngetChars(1);
    fsmTester->expectedError(false);
    lfsm->reset();

    lfsm->readChar(':');
    lfsm->readChar('=');
    lfsm->readChar('a');
    fsmTester->expectedToken(SP::IDENTIFIER);
    fsmTester->expectedUngetChars(1);
    fsmTester->expectedError(false);
    lfsm->readChar('a');//simulate ungetChar with second read with same char
    lfsm->readChar('b');
    lfsm->readChar('1');
    lfsm->readChar('+');

}

void test06(){
    fsmTester->testName = "test06";
    fsmTester->expectedToken(SP::SIGN);
    fsmTester->expectedUngetChars(1);
    fsmTester->expectedError(false);
    lfsm->reset();

    lfsm->readChar(':');
    lfsm->readChar('*');
    lfsm->readChar('a');

    lfsm->readChar('+');
    fsmTester->expectedToken(SP::NOTOKEN);
    fsmTester->expectedUngetChars(1);
    fsmTester->expectedError(false);
    lfsm->readChar('*');
    lfsm->readChar(':');
    lfsm->readChar('a');

}

void test07(){
    fsmTester->testName = "test07";
    fsmTester->expectedToken(SP::SIGN);
    fsmTester->expectedUngetChars(1);
    fsmTester->expectedError(false);
    lfsm->reset();

    lfsm->readChar(':');
    lfsm->readChar('*');
    lfsm->readChar('a');

    lfsm->readChar('+');
    fsmTester->expectedToken(SP::NOTOKEN);
    fsmTester->expectedUngetChars(1);
    fsmTester->expectedError(false);
    lfsm->readChar('*');
    lfsm->readChar('\0');
    lfsm->readChar('a');

}

void test08(){
    fsmTester->testName = "test08";
    fsmTester->expectedToken(SP::INTEGER);
    fsmTester->expectedUngetChars(1);
    fsmTester->expectedError(false);
    lfsm->reset();

    lfsm->readChar('1');

    lfsm->readChar('a');
    fsmTester->expectedToken(SP::IDENTIFIER);
    fsmTester->expectedUngetChars(1);
    fsmTester->expectedError(false);
    lfsm->readChar('a');//simulate ungetChar with second read with same char
    lfsm->readChar('b');
    lfsm->readChar('1');
    lfsm->readChar('+');

}

void test09(){
    fsmTester->testName = "test09";
    fsmTester->expectedToken(SP::INTEGER);
    fsmTester->expectedUngetChars(1);
    fsmTester->expectedError(false);
    lfsm->reset();

    lfsm->readChar('1');
lfsm->readChar('9');
    lfsm->readChar('a');
    fsmTester->expectedToken(SP::IDENTIFIER);
    fsmTester->expectedUngetChars(1);
    fsmTester->expectedError(false);
    lfsm->readChar('a');//simulate ungetChar with second read with same char
    lfsm->readChar('b');
    lfsm->readChar('1');
    lfsm->readChar('+');

}

void test10(){
    fsmTester->testName = "test10";
    fsmTester->expectedToken(SP::IDENTIFIER);
    fsmTester->expectedUngetChars(1);
    fsmTester->expectedError(false);
    lfsm->reset();

    lfsm->readChar('a');

    lfsm->readChar('+');
    fsmTester->expectedToken(SP::SIGN);
    fsmTester->expectedUngetChars(1);
    fsmTester->expectedError(false);
    lfsm->readChar('+');//simulate ungetChar with second read with same char

    lfsm->readChar('1');

}

void test11(){
    fsmTester->testName = "test11";
    fsmTester->expectedToken(SP::IDENTIFIER);
    fsmTester->expectedUngetChars(1);
    fsmTester->expectedError(false);
    lfsm->reset();

    lfsm->readChar('a');
lfsm->readChar('b');
    lfsm->readChar('+');
    fsmTester->expectedToken(SP::SIGN);
    fsmTester->expectedUngetChars(1);
    fsmTester->expectedError(false);
    lfsm->readChar('+');//simulate ungetChar with second read with same char

    lfsm->readChar('1');

}
void test12(){
    //long identifier
    fsmTester->testName = "test12";
    fsmTester->expectedToken(SP::IDENTIFIER);
    fsmTester->expectedUngetChars(1);
    fsmTester->expectedError(false);
    lfsm->reset();
 //   std::chrono::high_resolution_clock::time_point start = std::chrono::high_resolution_clock::now();
//int iterations = 86520;
    int iterations = 100;
    for (int k = 0; k < iterations; ++k) {
        //all uppercase letters
        for (int i = 65; i < 91; ++i) {
            lfsm->readChar(i);
        }
        //all lowercase letters
        for (int j = 97; j <123; ++j) {
            lfsm->readChar(j);
        }
    }



    //std::chrono::high_resolution_clock::time_point end = std::chrono::high_resolution_clock::now();
    //std::cout << "identifier length "<<iterations*(2*26)<< ": "<< std::chrono::duration_cast<std::chrono::microseconds>(end -start).count()<<" microseconds" << std::endl;



    lfsm->readChar('+');

}
void test14(){
    //long identifier
    fsmTester->testName = "test14";
    fsmTester->expectedToken(SP::INTEGER);
    fsmTester->expectedUngetChars(1);
    fsmTester->expectedError(false);
    lfsm->reset();
    //std::chrono::high_resolution_clock::time_point start = std::chrono::high_resolution_clock::now();
    int iterations = 1000000;
    for (int k = 0; k < iterations; ++k) {

        for (int i = 48; i < 57; ++i) {
            lfsm->readChar(i);
        }

    }

   // std::chrono::high_resolution_clock::time_point end = std::chrono::high_resolution_clock::now();
   // std::cout << "integer length "<<iterations*10<< ": "<< std::chrono::duration_cast<std::chrono::microseconds>(end -start).count()<<" microseconds" << std::endl;



    lfsm->readChar('a');


}

void test15(){
    fsmTester->testName = "test15";
    fsmTester->expectedToken(SP::NOTOKEN);
    fsmTester->expectedUngetChars(1);
    fsmTester->expectedError(false);
    lfsm->reset();

    lfsm->readChar('@');
    lfsm->readChar('1');

}

void test16(){
    fsmTester->testName = "test16";
    fsmTester->expectedToken(SP::NOTOKEN);
    fsmTester->expectedUngetChars(1);
    fsmTester->expectedError(false);
    lfsm->reset();

    lfsm->readChar('\0');
    lfsm->readChar('1');

}



void test17(){
delete lfsm;
}


int main (int argc, char* argv[]){



	//automat = new FSM();



     fsmTester = new FSMTester();
    lfsm = new LFSM(fsmTester);
    fsmTester->setLfsm(lfsm);

 // testSign01();
   /*   testSign02();
   testSign03();
      testSign04();
      testSign05();
      testSign06();
      testSign07();
      testSign08();
      testSign09();
     testDigit01();
     testDigit02();
      testIdentifier01();
      testIdentifier02();
      testIdentifier03();
      test01();
      test02();
      test03();
      test04();
      test05();
      test06();
      test07();
      test08();
      test09();
      test10();
      test11();
      test12();
      test14();
      test15();
      test16();*/
    fsmTester->testMode =false;
fsmTester->testWithBuffer();
    delete lfsm;
    delete fsmTester;
    printf("------");


}


