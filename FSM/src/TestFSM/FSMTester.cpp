//
// Created by Dominik on 22.10.2015.
//


#include "../../includes/TestFSM/FSMTester.h"
#include "../../../Buffer/includes/Buffer.h"

FSMTester::FSMTester() {
    this->testName = testName;
    tokenBuilder = new TokenBuilder();


}

FSMTester::~FSMTester() {

}

void FSMTester::testWithBuffer() {
  /* const char* testFile1 = "C:\\Users\\Dominik\\Documents\\workspaces\\CLion\\SysProgWS1516\\Testfiles\\scanner_tests\\scanner1.txt";
    char* testFile2 = "C:\\Users\\Dominik\\Documents\\workspaces\\CLion\\SysProgWS1516\\Testfiles\\scanner_tests\\scanner2.txt";
    char* testFile3 = "C:\\Users\\Dominik\\Documents\\workspaces\\CLion\\SysProgWS1516\\Testfiles\\scanner_tests\\scanner3.txt";
    char* testFile4 = "C:\\Users\\Dominik\\Documents\\workspaces\\CLion\\SysProgWS1516\\Testfiles\\scanner_tests\\scanner4.txt";
    char* testFile5 = "C:\\Users\\Dominik\\Documents\\workspaces\\CLion\\SysProgWS1516\\Testfiles\\scanner_tests\\scanner5.txt";
    char* testFile6 = "C:\\Users\\Dominik\\Documents\\workspaces\\CLion\\SysProgWS1516\\Testfiles\\scanner_tests\\scanner6.txt";
    char* testFile7 = "C:\\Users\\Dominik\\Documents\\workspaces\\CLion\\SysProgWS1516\\Testfiles\\scanner_tests\\scanner7.txt";
    char* testFile7_1 = "C:\\Users\\Dominik\\Documents\\workspaces\\CLion\\SysProgWS1516\\Testfiles\\scanner_tests\\scanner7_1.txt";
    char* testFile8 = "C:\\Users\\Dominik\\Documents\\workspaces\\CLion\\SysProgWS1516\\Testfiles\\scanner_tests\\scanner8.txt";
    char* testFile9 = "C:\\Users\\Dominik\\Documents\\workspaces\\CLion\\SysProgWS1516\\Testfiles\\scanner_tests\\scanner9.txt";*/








 //   buffer = new Buffer(
   //        testFile8);
  //  buffer = new Buffer(
    //       "C:\\Users\\Dominik\\Documents\\workspaces\\CLion\\SysProgWS1516\\Testfiles\\small_sourcecode.txt");

 //  buffer = new Buffer(
   //          "C:\\Users\\Dominik\\Documents\\workspaces\\CLion\\SysProgWS1516\\Testfiles\\small_text.txt");
    char c;
    /*do {
        c = buffer->getChar();

        lfsm->readChar(c);
    } while (c != '\0');*/
}

void FSMTester::setLfsm(LFSM *lfsm) {
    this->lfsm = lfsm;
}

void FSMTester::expectedToken(SP::TokenCategory expectedTokenType) {
    this->expectedTokenType = expectedTokenType;
}

void FSMTester::expectedUngetChars(int nr) {
    this->expectedUngetChar = nr;
}

void FSMTester::expectedError(bool errorExpected) {
    this->excpectedError = errorExpected;
}

void FSMTester::createToken(SP::TokenCategory tokenType) {
    //std::cout << "token found: "<<tokenType<<std::endl;
   /* char *name;

    int line = lfsm->getRowCount();

    int column = lfsm->getLinePosition();
   // printf("column: %i\n", column);
    int tokenLength = lfsm->getActualTokenLength();
   // printf("Token length: %i\n", tokenLength);
    posix_memalign((void **) &name, 64, (tokenLength + 1));

    this->buffer->copyLastCharactersToMemory(name, tokenLength);
    if (tokenType != SP::ERROR && tokenType != SP::NOTOKEN) {

        //printf("identifier: %s\n", name);
        Token newToken = this->tokenBuilder->buildToken(tokenType, name, line, column);
            std::cout << newToken << std::endl;
        }
    } else if(tokenType == SP::NOTOKEN){
     //   std::cout << "NoToken: " << name << std::endl;
    }else {
        std::cout << "Error: " << name << std::endl;
    }



    if (testMode) {
        if (expectedTokenType == tokenType) {
            //printf("Expected token type equals given token type: %i\n", tokenType);
            printf("Token in %s OK\n", testName);
        } else {
            printf("Expected token type %i but got %i\n", expectedTokenType, tokenType);
            printf("Token in %s FAILED\n", testName);
        }
    }*/

}

void FSMTester::ungetChar(int nr) {
    for (int i = 0; i < nr; ++i) {
        //  std::cout << "unget"<< nr <<std::endl;
 //       buffer->ungetChar();
    }

    if (testMode) {
        if (expectedUngetChar == nr) {
            printf("Ungetchar in %s OK\n", testName);
        } else {
            printf("Expected  ungetchar %i but got %i\n", expectedUngetChar, nr);
            printf("Ungetchar in %s FAILED\n", testName);
        }
    }

}

Token * FSMTester::nextToken(){
	return new Token(0,0,0);
}

