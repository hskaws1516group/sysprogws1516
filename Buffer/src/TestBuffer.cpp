#include "../includes/Buffer.h"
#include <iostream>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

int main(int argc, char **argv) {

	Buffer*  buffer;
	//const char* filename = "/home/thextor/Programme/Eclipse/workspace/SysProgTemplate_SS_15/Buffer/test.txt";
	const char* filename = "../Testfiles/scanner_tests/scanner9.txt";
//	const char* filename = "C:\\Users\\Dominik\\Documents\\workspaces\\CLion\\SysProgWS1516\\Testfiles\\2.txt";

	try{
		buffer = new Buffer(filename);

		char c;
		int i = 0;
		int k = 1030;
		int wordLength = rand() % k + 1;

		std::cout << "------------------------------" << std::endl;

		do{
			c = buffer->getChar();
			//std::cout << c;
			i++;			
						
			if (i >= wordLength) {
				char* name;
				posix_memalign((void **)&name, 64, (i + 1));
				buffer->copyLastCharactersToMemory(name, i);
				i = 0;
				//std::cout << name;
				wordLength = rand() % k + 1;
			}
			
			
		}while(c != '\0');

		std::cout << std::endl << "------------------------------" << std::endl;
	}
	catch (const char* e){
		std::cout << e << std::endl;
		return 0;
	}
}
