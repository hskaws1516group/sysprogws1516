#include "../includes/Buffer.h"

#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <iostream>
#include <stdio.h>

#define BUFFER_SIZE 		2048
#define BUFFER_ALIGNMENT	2048

Buffer::Buffer(const char* filename) {
	this->index 	  		  = -1;
	this->bufferRound 	      = 0;
	this->prevBufferAvailable = false;
	this->filename			  = filename;

	// allocate memory for both buffers, throw exception when it fails
	if (posix_memalign((void **)&this->buffer1, BUFFER_ALIGNMENT, BUFFER_SIZE) != 0 ||
		posix_memalign((void **)&this->buffer2, BUFFER_ALIGNMENT, BUFFER_SIZE) != 0){
		throw "Buffer could not allocate memory!";
	}

	// open file, throw exception when it fails
	this->openFile();

	this->loadNextBuffer(this->buffer1, true);
}

Buffer::~Buffer() {
	close(this->fileHandle);
	free(this->buffer1);
	free(this->buffer2);
}

int Buffer::getBufferSize(char* buffer){
	if (this->buffer1 == buffer){
		return this->buffer1DataSize;
	}
	
	return this->buffer2DataSize;
}

void Buffer::loadNextBuffer(char* buffer, bool setActive){		
	if (this->buffer1 == buffer){
		this->buffer1DataSize = read(this->fileHandle, buffer, BUFFER_SIZE);
	}
	else{
		this->buffer2DataSize = read(this->fileHandle, buffer, BUFFER_SIZE);
	}
	
	this->bufferRound++;
		
	this->prevBufferAvailable = this->bufferRound == 1 ? false : true;
	this->nextBufferAvailable = false;

	if (setActive){
		this->activeBuffer = buffer;
	}
}

void Buffer::openFile(){
	this->fileHandle = open(this->filename, O_DIRECT);

	if (this->fileHandle == -1){
		throw "Buffer could not open the file!";
	}
}

void Buffer::resetToBufferRound(int round){	
	this->index 	  		  = -1;
	this->bufferRound 	      = 0;
	this->prevBufferAvailable = false;
	this->nextBufferAvailable = false;

	close(this->fileHandle);

    // open file, throw exception when it fails
    this->fileHandle = open(this->filename, O_DIRECT);

	if (this->fileHandle == -1){
		throw "Buffer could not open the file!";
	}

	for(int i = 0; i <= round; i++){
		this->loadNextBuffer(i % 2 == 0 ? this->buffer1 : this->buffer2, true);
	}
}

char Buffer::getChar() {
	this->index++;
	char c = this->activeBuffer[this->index];
	if (c == '\0'){
		// end of buffer, load next
		if (this->index == BUFFER_SIZE){

			this->activeBuffer = (this->activeBuffer == this->buffer1 ? this->buffer2 : this->buffer1);

			// next buffer may already exists, no need to read it again
			if (!this->nextBufferAvailable){
				this->loadNextBuffer(this->activeBuffer, true);
			}
			else{
				this->prevBufferAvailable = true;
				this->nextBufferAvailable = false;
			}

			// still data left, get next char from new buffer
			if (this->getBufferSize(this->activeBuffer) != 0){
				this->index = -1;
				return this->getChar();
			}
		}
		// null in the middle of the file, change to blank
		else if (this->index < this->getBufferSize(this->activeBuffer)){
			c = ' ';
		}
	}	
	
	return c;
}

void Buffer::ungetChar(){
	this->index--;
	
	if (this->index < 0){

		// start of the file
		if (this->bufferRound == 1){
			return;
		}

		// previous buffer is available
		if (this->prevBufferAvailable){
			this->prevBufferAvailable = false;
			this->nextBufferAvailable = true;

			this->activeBuffer = (this->activeBuffer == this->buffer1) ? this->buffer2 : this->buffer1;
			this->bufferRound--;
		}
		// can not read previous buffer, reset to bufferRound - 1
		else {
			//std::cout << std::endl << std::endl << "------------------------------------ resetToRound (" << this->bufferRound << ") ------------------------------------" << std::endl << std::endl;
			this->resetToBufferRound(this->bufferRound - 1);
		}

		this->index = BUFFER_SIZE - 1;
	}
}

void Buffer::copyLastCharactersToMemory(char* dest, int charCount){
	for(int i = 0; i < charCount; i++){
		this->ungetChar();
	}
	
	for(int i = 0; i < charCount; i++){
		*dest = this->getChar();
		 dest++;
	}
	
	*dest = '\0';
}
