/*
 * Scanner.cpp
 *
 *  Created on: Sep 26, 2012
 *      Author: knad0001
 */

#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <iostream>
#include "../includes/Scanner.h"
#include "../../Token/includes/Token.h"
#include "../../Token/includes/TokenBuilder.h"
#include "../../utilities/includes/Output.h"

using namespace std;

Scanner::Scanner(const char *filename, Symboltable* symtab) {
	this->buffer = new Buffer(filename);
	this->tokenBuilder = new TokenBuilder();
	this->fsm = new LFSM(this);

	this->symtab = symtab;
	fileEnd = false;

}

Scanner::~Scanner() {

}

void Scanner::createToken(Tokens::TokenCategory tokenCategory) {
	/*const*/char *name;

	int linePos = fsm->getLinePosition();
	int row = fsm->getRowCount();
	int tokenLength = fsm->getActualTokenLength();

	if (tokenCategory != Tokens::TC_NOTOKEN) {
		//std::cout << "token length: "<<tokenLength << std::endl;
		posix_memalign((void **) &name, 64, (tokenLength + 1));
		this->buffer->copyLastCharactersToMemory(name, tokenLength);

		//build token
		Token* newToken = this->tokenBuilder->buildToken(tokenCategory, name,
				row, linePos);

		if (newToken->getType() == Tokens::TokenType::T_ERROR) {

			Output::printErrorToken(newToken);
		} else {

			Output::printToken(newToken);

			symtab->insert(name, newToken);

		}
		tokenFound = true;
		foundToken = newToken;
	}
}

void Scanner::ungetChar(int nr) {
	for (int i = 0; i < nr; i++) {
		this->buffer->ungetChar();
	}
}

void Scanner::init() {
	Token* whileToken = tokenBuilder->buildToken(Tokens::TC_IDENTIFIER, "WHILE",
			0, 0);
	symtab->insert("WHILE", whileToken);
	symtab->insert("while", whileToken);
	Token* ifToken = tokenBuilder->buildToken(Tokens::TC_IDENTIFIER, "IF", 0,
			0);
	symtab->insert("IF", ifToken);
	symtab->insert("if", ifToken);


	Token* elseToken = tokenBuilder->buildToken(Tokens::TC_IDENTIFIER, "ELSE", 0,
				0);
		symtab->insert("ELSE", elseToken);
		symtab->insert("else", elseToken);

	Token* writeToken = tokenBuilder->buildToken(Tokens::TC_IDENTIFIER, "write",0,0);
	symtab->insert("write", writeToken);

	Token* readToken = tokenBuilder->buildToken(Tokens::TC_IDENTIFIER, "read", 0,0);
	symtab->insert("read", readToken);

	Token* intToken = tokenBuilder->buildToken(Tokens::TC_IDENTIFIER, "int", 0,0);

	symtab->insert("int", intToken);



}

bool Scanner::isFileEnd() {
	return fileEnd;
}

Token* Scanner::nextToken() {
	char c;
	tokenFound = false;
	do {
		c = this->buffer->getChar();
		fsm->readChar(c);
		if (c == '\0') {
			fileEnd = true;
		}

	} while (c != '\0' && !tokenFound);
	return foundToken;
}
