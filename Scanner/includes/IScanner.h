//
// Created by Dominik on 22.10.2015.
//

#ifndef ISCANNER_H
#define ISCANNER_H


#include "../../Token/includes/Token.h"
/**
 * Interface with necessarily needed methods of a scanner
 */
class IScanner {
public:

	/**
	 * Creates a new token out of the given TokenCategory.
	 * @param TokenType the category of the token which should be created
	 */
	virtual void createToken(Tokens::TokenCategory tokenType) = 0;
	/**
	 * Sets the buffer a specific number of characters back
	 * @param nr the number of chars the buffer should go back
	 */
	virtual void ungetChar(int nr) = 0;
	/**
	 * Processes characters from  the given textfile as long as there is no token found. If a token was found, it will be
	 * returned.
	 * @return a token, if found
	 */
	virtual Token* nextToken() =0;

};

#endif //ISCANNER_H
