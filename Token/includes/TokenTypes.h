//
// Created by Dominik on 02.11.2015.
//

#ifndef SYSPROGWS1516_TOKENTYPES_H
#define SYSPROGWS1516_TOKENTYPES_H
/**
 * Namespace which contains different enums which are used in the context of tokens
 */
namespace Tokens {
	/**
	 * The precise type of a token.
	 */
    enum TokenType {
        T_IDENTIFIER = 0,
        T_INTEGER = 1,
        S_PLUS = 2,
        S_MINUS = 3,
        S_COLON = 4,
        S_STAR = 5,
        S_LESS = 6,
        S_GREATER = 7,
        S_EQUAL = 8,
        S_ASSIGN = 9,
        S_LESSCOLONGREATER = 10,
        S_EXCLAMATION = 11,
        S_AMP = 12,
        S_SEMICOLON = 13,
        S_BRACKET_OPEN = 14,
        S_BRACKET_CLOSE = 15,
        S_CURLY_BRACKET_OPEN = 16,
        S_CURLY_BRACKET_CLOSE = 17,
        S_SQUARE_BRACKET_OPEN = 18,
        S_SQUARE_BRACKET_CLOSE = 19,
        T_UNKNOWN = 20,
		T_WHILETOKEN = 21,
		T_IFTOKEN = 22,
		T_ERROR = 23,
		T_WRITETOKEN = 24,
		T_READTOKEN = 25,
		T_INTTOKEN = 26,
		T_ELSETOKEN = 27


    };

	/**
	 * The tokencategories used by the FSM.
	 */
	enum TokenCategory {
		TC_IDENTIFIER = 0,
		TC_INTEGER = 1,
		TC_SIGN = 2,
		TC_ERROR = 3,
		TC_NOTOKEN = 4,


	};

}
#endif //SYSPROGWS1516_TOKENTYPES_H
