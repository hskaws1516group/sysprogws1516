//
// Created by Dominik on 11.11.2015.
//

#ifndef SYSPROGWS1516_TOKENBUILDER_H
#define SYSPROGWS1516_TOKENBUILDER_H

#include "TokenTypes.h"

#include "Token.h"

/**
 * The tokenbuilder builds appropriate tokens based on the given token content.
 * It decides whether a token is a sign, identifier, integer or error token.
 */
class TokenBuilder {

public:
    /**
     * Builds a new token based on the given tokendata
     * @param category the category of the token provided by the FSM
     * @param c the content of the token from the file
     * @param line the line in which the token was found in the file
     * @param column the column in which the token starts in the line.
     */
    Token* buildToken(Tokens::TokenCategory category,const char* c,  int line, int column);

private:
    Tokens::TokenType signTokenCharToType(const char *c);
    Tokens::TokenType checkForKeywordToken(const char* tokenName);

};


#endif //SYSPROGWS1516_TOKENBUILDER_H
