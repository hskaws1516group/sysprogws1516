//
// Created by Dominik on 11.11.2015.
//

#include "../includes/TokenBuilder.h"
#include "../../utilities/includes/StringCmp.h"
#include <stdlib.h>
#include <stdio.h>
#include<cerrno>
#include<limits.h>
Token* TokenBuilder::buildToken(Tokens::TokenCategory category, const char *c, int line, int column) {
    if(category == Tokens::TC_INTEGER){
    	int number = strtol(c,NULL,10);

    	if(errno == 34 || number < 0){

    		std::cerr << "Value range of integer exceeded in line "<<line << " column "<<column << std::endl;
    		errno = 0;
    		return new Token(Tokens::TokenType::T_ERROR, line, column, c);

    	}
        return new Token(line, column,number);

    }else if(category == Tokens::TC_SIGN){


        return new Token(signTokenCharToType(c), line, column);
    }else if(category == Tokens::TC_ERROR){
    	return new Token(Tokens::TokenType::T_ERROR, line, column, c);
    }
    else {
    	//check if the token is a keyword
    	Tokens::TokenType realType = checkForKeywordToken(c);


        return new Token(realType, line, column, c);
    }

}

Tokens::TokenType TokenBuilder::signTokenCharToType(const char *c) {

    switch (*c) {
        case '+':
            return Tokens::S_PLUS;
            break;
        case '-':
            return Tokens::S_MINUS;
            break;
        case ':':
        {
            c++;

            if(*c == '='){
                return Tokens::S_ASSIGN;
            }else {
                return Tokens::S_COLON;
            }
            break;}
        case '*':
            return Tokens::S_STAR;
            break;
        case '<':
        {     c++;

            if(*c == ':'){
                 c++;
                if(*c == '>'){
                    return Tokens::S_LESSCOLONGREATER;
                }else {
                    throw "TokenBuilder: Invalid token content.";
                }
            }else {
                return Tokens::S_LESS;
            }
            break;}

        case '>':
            return Tokens::S_GREATER;
            break;
        case '=':
            return Tokens::S_EQUAL;
            break;
        case '!':
            return Tokens::S_EXCLAMATION;
            break;
        case '&':
            return Tokens::S_AMP;
            break;
        case ';':
            return Tokens::S_SEMICOLON;
            break;
        case '(':
            return Tokens::S_BRACKET_OPEN;
            break;
        case ')':
            return Tokens::S_BRACKET_CLOSE;
            break;
        case '{':
            return Tokens::S_CURLY_BRACKET_OPEN;
            break;
        case '}':
            return Tokens::S_CURLY_BRACKET_CLOSE;
            break;
        case '[':
            return Tokens::S_SQUARE_BRACKET_OPEN;
            break;
        case ']':
            return Tokens::S_SQUARE_BRACKET_CLOSE;
            break;
        default:
            return Tokens::T_UNKNOWN;
            break;


    }




}
   Tokens::TokenType TokenBuilder::checkForKeywordToken(const char* tokenName) {

    	if(StringCmp::equals(tokenName, "WHILE") || StringCmp::equals(tokenName, "while")){
    		return Tokens::T_WHILETOKEN;
    	}else if(StringCmp::equals(tokenName, "if") || StringCmp::equals(tokenName, "IF")){
    		return Tokens::T_IFTOKEN;
    	}else if(StringCmp::equals(tokenName, "read")){
    		return Tokens::T_READTOKEN;
    	}else if(StringCmp::equals(tokenName, "write")){
    		return Tokens::T_WRITETOKEN;
    	}else if(StringCmp::equals(tokenName, "int")){
    		return Tokens::T_INTTOKEN;
    	}else if(StringCmp::equals(tokenName, "else")){
    		return Tokens::T_ELSETOKEN;
    	}

        return Tokens::T_IDENTIFIER;

    }
