//
// Created by Dominik on 02.11.2015.
//

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "../includes/Token.h"

//For output formatting
#include <iomanip>

const char* const Token::tokenTypeNames[] = {"Identifier","Integer", "Plus","Minus","Colon","Star","Less than","Greater than",
"Equal","Assign","LessColonGreater","Exclamationmark","Ampersand","Semicolon","Bracket open","Bracket closing","Curly bracket open","Curly bracket close",
"Square bracket open", "Square bracket close","Unknown", "WHILE","IF","error","write","read", "int", "else"};
const int Token::maxTokenNameWidth = 20;

Token::Token(Tokens::TokenType type, int line, int column) : identifierName(), integerValue(){

    this->line = line;
    this->column = column;
    this->type = type;
    this->infoType = InformationType::noType;
}

Token::Token(int line, int column, int integerValue) : identifierName(){
    this->type = Tokens::T_INTEGER;
    this->line = line;
    this->column = column;
    this->integerValue = integerValue;
    this->infoType = InformationType::noType;
}

Token::Token(Tokens::TokenType type, int line, int column, const char* identifier) : integerValue(){
	this->type = type;
    this->line = line;
    this->column = column;

    identifierName = identifier;
    this->infoType = InformationType::noType;
}

Token::~Token(){

	free((char*)identifierName);
}

const char* Token::getIdentifier(){
    return this->identifierName;
}

int Token::getIntegerValue(){
    return this->integerValue;
}

Tokens::TokenType Token::getType(){
	return type;
}

void Token::printSimple() {
	if (type == Tokens::T_IDENTIFIER) {
		std::cout << identifierName;
	} else if (type == Tokens::T_INTEGER) {
		std::cout << integerValue;
	} else {
		std::cout << Token::tokenTypeNames[type];
	}
}

void Token::setInfoType(InformationType::InfoType infoType){
    //std::cout << "setInfoType: " << infoType << std::endl;
    this->infoType = infoType;
}

InformationType::InfoType Token::getInfoType(){
    return this->infoType;
}

std::ostream &operator<<(std::ostream &out, Token &token) {
	if(token.type != Tokens::T_ERROR){
		out << "Token " << std::setw(Token::maxTokenNameWidth) << Token::tokenTypeNames[token.type]
																						<< " Line: " << std::setw(5) << token.line
																						<< " Column: " << std::setw(5) << token.column;

		if (token.type == Tokens::T_IDENTIFIER) {
			out << " Lexem: "<< token.identifierName;

		} else if (token.type == Tokens::T_INTEGER){
			out << " Value: "<< token.integerValue;

		}
	}
	else {
		out << "unknown Token "
				<< " Line: " << std::setw(5) << token.line
				<< " Column: " << std::setw(5) << token.column  << " Symbol: "<<token.identifierName;
	}
    return out;
}
