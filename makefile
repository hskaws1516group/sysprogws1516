

OBJDIR = objs

TOKENDIR = Token
AUTOMATDIR = FSM
BUFFERDIR = Buffer
SYMBOLTABLEDIR = Symboltable
SCANNERDIR = Scanner
UTILITIESDIR = utilities
PROJECTDIR = projectOBJs

all: tokenOBJs fsmOBJs bufferOBJs symboltableOBJs scannerOBJs mainOBJs utilitiesOBJs

cleanAll:
	rm -f $(TOKENDIR)/$(OBJDIR)/*.o
	rm -f $(AUTOMATDIR)/$(OBJDIR)/*.o
	rm -f $(BUFFERDIR)/$(OBJDIR)/*.o
	rm -f $(SYMBOLTABLEDIR)/$(OBJDIR)/*.o
	rm -f $(SCANNERDIR)/$(OBJDIR)/*.o
	rm -f $(SCANNERDIR)/debug/*
	rm -f $(PROJECTDIR)/*.o
	rm -f $(UTILITIESDIR)/$(OBJDIR)/*.o
	rm -f RunScanner

utilitiesOBJs:
	$(MAKE) -C $(UTILITIESDIR) UtilitiesOBJTarget
	
tokenOBJs:
	$(MAKE) -C $(TOKENDIR) TokenOBJTarget

fsmOBJs:
	$(MAKE) -C $(AUTOMATDIR) FSMOBJTarget
	
	
bufferOBJs:
	$(MAKE) -C $(BUFFERDIR) BufferOBJTarget

	
symboltableOBJs:
	$(MAKE) -C $(SYMBOLTABLEDIR) SymTabOBJTarget
	

scannerOBJs: 
	$(MAKE) -C $(SCANNERDIR) ScannerOBJTarget

mainOBJs: main.cpp
	g++ -std=c++11 -g -c -Wall -o $(PROJECTDIR)/Main.o main.cpp
	
	
#link everything
Scanner: FSM/objs/FSM.o Buffer/objs/Buffer.o Symboltable/objs/Symboltable.o Scanner/objs/Scanner.o utilities/objs/StringCmp.o utilities/objs/Output.o 
	g++ -std=c++11 -o RunScanner utilities/objs/Output.o utilities/objs/StringCmp.o projectOBJs/Main.o FSM/objs/FSM.o FSM/objs/LFSM.o Buffer/objs/Buffer.o Symboltable/objs/Symboltable.o Token/objs/Token.o Scanner/objs/Scanner.o Token/objs/TokenBuilder.o

ScannerAll: 
	${MAKE} cleanAll
	${MAKE} all
	g++ -std=c++11 -o RunScanner utilities/objs/Output.o utilities/objs/StringCmp.o projectOBJs/Main.o FSM/objs/FSM.o FSM/objs/LFSM.o Buffer/objs/Buffer.o Symboltable/objs/Symboltable.o Token/objs/Token.o Scanner/objs/Scanner.o Token/objs/TokenBuilder.o

ScannerLib:
	${MAKE} cleanAll
	${MAKE} all
	ld -r utilities/objs/Output.o utilities/objs/StringCmp.o FSM/objs/FSM.o FSM/objs/LFSM.o Buffer/objs/Buffer.o Symboltable/objs/Symboltable.o Token/objs/Token.o Scanner/objs/Scanner.o Token/objs/TokenBuilder.o -o ScannerLib.o

