/*
 * StringCmp.h
 *
 *  Created on: 09.12.2015
 *      Author: benjamin
 */

#ifndef UTILITIES_SRC_STRINGCMP_H_
#define UTILITIES_SRC_STRINGCMP_H_

/**
 * Provides a method to compare two C strings
 * \author Benjamin Büscher
 */
namespace StringCmp {

	/**
	 * Checks if both char arrays contain the same values.
	 * @param s1 first string to be compared.
	 * @param s2 second string to be compared.
	 * @return true if s1 equals s2
	 */
	bool equals(const char* s1, const char* s2);
};

#endif /* UTILITIES_SRC_STRINGCMP_H_ */
