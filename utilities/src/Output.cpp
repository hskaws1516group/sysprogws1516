/*
 * Output.cpp
 *
 *  Created on: 09.12.2015
 *      Author: benjamin
 */

#include "../includes/Output.h"
#include "../../Token/includes/Token.h"
#include <fstream>
#include <iomanip>

std::ofstream Output::file;

Output::Output() {}

Output::~Output() {
	Output::close();
}

void Output::init(const char* filePath) {
	if (!Output::file.is_open()) {
		Output::file.open(filePath);
		Output::file << std::left;
		std::cerr << std::left;
	}
}

void Output::close() {
	if (Output::file.is_open()) {
		Output::file << std::flush;
		Output::file.close();
	}
	std::cerr << std::flush;
}

void Output::printToken(Token* token) {
	if (Output::file == 0) {
		std::cerr << "Error: You have to call Output::init() before using Output::printToken()!" << std::endl;
	} else if (token != 0) {
		Output::file << *token << "\n";
		//std::cout << *token << "\n";
	}
}

void Output::printErrorToken(Token* token) {
	if (Output::file == 0) {
		std::cerr << std::left << "Warning: You called Output::printErrorToken() before initialization of Output!" << std::endl;
	}
	if (token != 0) {
		std::cerr << *token << "\n";
	}

	throw "Scanner Error";
}
