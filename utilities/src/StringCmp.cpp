/*
 * StringCmp.cpp
 *
 *  Created on: 09.12.2015
 *      Author: benjamin
 */

#include "../includes/StringCmp.h"

bool StringCmp::equals(const char* s1, const char* s2) {
	// one or both strings are null
	if (s1 == 0 || s2 == 0) {
		if (s1 == 0 && s2 == 0) {
			return true;
		} else {
			return false;
		}
	}
	// both strings exist, compare them
	int i = 0;
	while (s1[i] != 0 || s2[i] != 0) {
		if (s1[i] != s2[i]) {
			return false;
		}
		i++;
	}
	return true;
}
