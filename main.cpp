#include <iostream>
#include "Scanner/includes/Scanner.h"
#include "utilities/includes/Output.h"
#include <unistd.h>

using namespace std;

int main(int argc, char* argv[]) {

	if (argc != 3) {
		cout << "Bitte das Programm so aufrufen: " << "\n";
		cout << "./RunScanner <input.txt> <output.txt>" << endl;

		return 0;
	}

	char* inputFile = argv[1];
	char* outputFile = argv[2];

	cout << "Input: " << inputFile << "\n";
	cout << "Output: " << outputFile << "\n";
	cout << "------------------------" << "\n" << endl;
	try {
		Symboltable* symtab = new Symboltable(10000);
		Output::init(outputFile);
		Scanner* scanner = new Scanner(inputFile, symtab);
		scanner->init();

		do {
		Token* t = scanner->nextToken();
			//delete t;
		} while (!scanner->isFileEnd());

		cout << "\n------------------------" << endl;
		symtab->printStats();

	} catch (const char* e) {
		cout << "Error" << "\n" << "-----" << "\n";
		cout << e << endl;
		return 0;
	}
	Output::close();

	return 0;
}
