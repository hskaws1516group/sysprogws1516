#include <iostream>

#include "../includes/Symboltable.h"
#include "../../Token/includes/Token.h"

int main(int argc, char **argv) {
	std::cout << "Test Symboltable" << std::endl;
	Symboltable symtable = Symboltable(3001);
	symtable.insert("ident", new Token(5, 6, "ident"));
	symtable.insert("ident", new Token(7, 8, "identfail"));
	std::cout << "Existiert ident? " << symtable.contains("ident") << " " << *symtable.lookup("ident") << std::endl;
	std::cout << "Existiert identfail? " << symtable.contains("identfail") << std::endl;
	symtable.insert("identfail", new Token(9, 10, "identfail"));
	std::cout << "Existiert identfail? " << symtable.contains("identfail") << " " << *symtable.lookup("identfail") << std::endl;
	std::cout << "Existiert keys? " << symtable.contains("keys") << std::endl;
	symtable.insert("Baum", new Token(0, 0, ""));
	symtable.insert("kartoffel", new Token(0, 0, ""));
	symtable.insert("flussschifffahrt", new Token(0, 0, ""));
	symtable.insert("ich", new Token(0, 0, ""));
	symtable.insert("du", new Token(0, 0, ""));
	symtable.insert("er", new Token(0, 0, ""));
	symtable.insert("sie", new Token(0, 0, ""));
	symtable.insert("es", new Token(0, 0, ""));
	symtable.insert("wir", new Token(0, 0, ""));
	symtable.insert("ihr", new Token(0, 0, ""));
	symtable.insert("sie", new Token(0, 0, ""));
	symtable.insert("wenn", new Token(0, 0, ""));
	symtable.insert("while", new Token(0, 0, ""));
	symtable.insert("WHILE", new Token(0, 0, ""));
	symtable.insert("if", new Token(0, 0, ""));
	symtable.insert("IF", new Token(0, 0, ""));
	symtable.insert("i", new Token(0, 0, ""));
	symtable.insert("k", new Token(0, 0, ""));
	symtable.insert("l", new Token(0, 0, ""));
	symtable.insert("m", new Token(0, 0, ""));
	symtable.insert("counter", new Token(0, 0, ""));
	symtable.insert("zaehler", new Token(0, 0, ""));
	symtable.insert("rechne", new Token(0, 0, ""));
	symtable.insert("calc", new Token(0, 0, ""));
	symtable.insert("do", new Token(0, 0, ""));
	symtable.insert("end", new Token(0, 0, ""));
	symtable.insert("begin", new Token(0, 0, ""));
	symtable.insert("something", new Token(0, 0, ""));
	symtable.insert("cool", new Token(0, 0, ""));

	symtable.printStats();


	/*
	 * g++ -std=c++11 -g  -c -Wall src/Symboltable.cpp -o objs/Symboltable.o
	 * g++ -std=c++11 -g  -c -Wall src/TestSymboltable.cpp -o objs/TestSymboltable.o
	 * g++ -std=c++11 -g  -c -Wall ../Token/src/Token.cpp -o ../Token/objs/Token.o
	 * g++ -std=c++11 -g ../Token/objs/Token.o objs/Symboltable.o objs/TestSymboltable.o  -o debug/SymboltableTest
	 */
}
