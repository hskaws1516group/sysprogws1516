/*
 * SymbolTableE.cpp
 *
 *  Created on: 11.11.2015
 *      Author: benjamin
 */

#ifndef SYMBOLTABLE_SRC_SYMBOLTABLEE_CPP_
#define SYMBOLTABLE_SRC_SYMBOLTABLEE_CPP_
#include "../includes/Hashtable.h"
#include "../../Token/includes/Token.h"

#include <iostream>


template<class Value> Hashtable<Value>::Hashtable(int size) {
	map = new LinkedList<Value>*[size];
	this->size = size;
	for (int i = 0; i < size; i++) {
		map[i] = new LinkedList<Value>();
	}
}

template<class Value> Hashtable<Value>::~Hashtable() {
	delete [] map;
}

template<class Value> unsigned int Hashtable<Value>::generateHash(const char* key) {
	unsigned int hash = 239 + key[0];
	for (unsigned int counter = 1; key[counter] != 0; counter++) {
		hash = hash * 19 + key[counter];
	}
	return hash % size;
}

template<class Value> bool Hashtable<Value>::contains(const char* key) {
	return lookup(key) != 0;
}

template<class Value> void Hashtable<Value>::insert(const char* key, Value* value) {
	LinkedList<Value>* list = map[generateHash(key)];
	if (!list->exists(key)) {
		list->add(key, value);
	}
}

template<class Value> Value* Hashtable<Value>::lookup(const char* key) {
	LinkedList<Value>* list = map[generateHash(key)];
	return list->get(key);
}

template<class Value> void Hashtable<Value>::printStats() {
	// total number of elements in the hashtable
	int itemcount = 0;
	// number of elements added to a list already containing something
	int collisions = 0;
	// maximum length of one list
	int maxListSize = 0;
	// number of elements in the list no i
	int tmpsize;
	for (int i = 0; i < size; i++) {
		tmpsize = map[i]->countElements();
		itemcount += tmpsize;
		if (tmpsize) {
			collisions += tmpsize - 1;
			if (tmpsize > maxListSize) {
				maxListSize = tmpsize;
			}
		}
	}
	double loadfactor = itemcount * 1.0 / size;
	std::cout << "Hashtable has a size of " << size <<
			". \nIt contains " << itemcount << " elements (Load factor: " << loadfactor << "). \n"
			<< collisions << " collisions occured. \nLongest List contains " << maxListSize << " elements.\n"<< std::endl;
}

#endif /* SYMBOLTABLE_SRC_SYMBOLTABLEE_CPP_ */
