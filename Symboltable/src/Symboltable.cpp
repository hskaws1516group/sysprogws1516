/*
 * Symboltable.cpp
 *
 *  Created on: 12.11.2015
 *      Author: benjamin
 */

#include "../includes/Symboltable.h"

Symboltable::Symboltable(int size) {
	symtable = new Hashtable<Token>(size);
}

Symboltable::~Symboltable(){
	delete symtable;
}

bool Symboltable::contains(const char* lexem){
	return symtable->contains(lexem);
}

void Symboltable::insert(const char* lexem, Token* token){
	symtable->insert(lexem, token);
}

Token* Symboltable::lookup(const char* lexem){
	return symtable->lookup(lexem);
}

//stats
void Symboltable::printStats(){
	symtable->printStats();
}
