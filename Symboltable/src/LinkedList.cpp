/*
 * LinkedListE.cpp
 *
 *  Created on: 11.11.2015
 *      Author: benjamin
 */

#ifndef SYMBOLTABLE_SRC_LINKEDLISTE_CPP_
#define SYMBOLTABLE_SRC_LINKEDLISTE_CPP_
#include "../includes/LinkedList.h"
#include <iostream>
#include "../../utilities/includes/StringCmp.h"
#include "../../Token/includes/Token.h"

template<class Value> LinkedList<Value>::LinkedList() : first() {

}

template<class Value> LinkedList<Value>::~LinkedList() {
	delete first;
}

template<class Value> void LinkedList<Value>::add(const char* key, Value* value) {
	ListElement* addme = new ListElement(key, value);
	addme->next = first;
	first = addme;
}

template<class Value> bool LinkedList<Value>::exists(const char* key) {
	return get(key) != 0;
}

template<class Value> Value* LinkedList<Value>::get(const char* key) {
	ListElement* current = first;
	while (current != 0) {
		if (StringCmp::equals(current->key, key)) {
			return current->value;
		}
		current = current->next;
	}
	return 0;
}

// needed to print hashtable stats
template<class Value> int LinkedList<Value>::countElements() {
	int elements = 0;
	ListElement* current = first;
	while (current != 0) {
		elements++;
		current = current->next;
	}
	return elements;
}

template<class Value> LinkedList<Value>::ListElement::ListElement(const char* key, Value* value) : next() {
	this->key = key;
	this->value = value;
}

template<class Value> LinkedList<Value>::ListElement::~ListElement() {
	delete next;
	// can't delete content cause not every token is added to Hashtable
	// maybe i'll copy the key when inserting. In this case I'd have to delete it here.
	//delete key;
}
template class LinkedList<Token>;
#endif /* SYMBOLTABLE_SRC_LINKEDLISTE_CPP_ */
