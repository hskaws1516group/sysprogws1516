/*
 * SymbolTableE.h
 *
 *  Created on: 11.11.2015
 *      Author: benjamin
 */

#ifndef SYMBOLTABLE_SRC_SYMBOLTABLEE_H_
#define SYMBOLTABLE_SRC_SYMBOLTABLEE_H_
#include "LinkedList.h"

template <class Value>

/**
 * A hashtable to store generic key-value pairs.
 * value is generic, key is fixed to const char*.
 * @author Benjamin Büscher
 */
class Hashtable {
public:

	/**
	 * Creates a new hashtable with the given size.
	 * @param size the size for the hashtable
	 */
	Hashtable(int size);

	/**
	 * deletes the hashtable
	 */
	virtual ~Hashtable();

	/**
	 * checks if the hashtable contains a key-value pair with the given key.
	 * @param key key to be searched for.
	 * @return true if the hashtable contains a fitting key-value pair.
	 */
	bool contains(const char* key);

	/**
	 * Inserts the key-value pair in the hashtable.
	 * If the hashtable already contains the key nothing is done.
	 * @param key the key to be associated with the value.
	 * @param value the value to be stored associated with the key.
	 */
	void insert(const char* key, Value* value);

	/**
	 * Returns the value associated with the given key.
	 * @param key the key to get associated value to.
	 * @return the value associated with the given key. null if key doesn't exist.
	 */
	Value* lookup(const char* key);

	/**
	 * prints stats of the hashtable to cout.
	 */
	void printStats();
private:
	unsigned int generateHash(const char* key);
	int size;
	LinkedList<Value>** map;
};
#include "../src/Hashtable.cpp"

#endif /* SYMBOLTABLE_SRC_SYMBOLTABLEE_H_ */
